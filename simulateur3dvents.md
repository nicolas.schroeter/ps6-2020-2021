---
version: 2
titre: Simulateur 3D de vents
type de projet: Projet de semestre 6
année scolaire: 2020/2021
abréviation: 3D Vents
filières:
  - Télécommunications
  - Informatique
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Richard Baltensperger
mots-clé: [moteur jeu 3D, modélisation de vents, casque de réalité virtuelle]
langue: [F]
confidentialité: non
suite: non
---

\begin{center}
\includegraphics[width=0.7\textwidth]{img/espaceballon.jpg}
\end{center}

## Contexte/Objectifs
Espace Ballon (https://www.espace-ballon.ch) est une fondation située à Château d’Oex dont l’objectif est de promouvoir les ballons à air chaud ainsi que la région. 

Pour augmenter leur offre d’activités dans leurs locaux, Espace Ballon désire disposer d’un simulateur virtuel 3D de ballon à air chaud de niveau professionnel qui permet au public de s’initier au pilotage et aux professionnels d’effectuer des heures de vol d’entrainement.

Pour atteindre une qualité professionnelle du simulateur, il est nécessaire de disposer de modélisations très réalistes des différentes situations des vents dans la vallée du Pays-d’Enhaut. 

Ce projet a pour but de développer un simulateur 3D qui 

-  permet l’édition, la visualisation et l’enregistrement de plusieurs modèles de vents 
-  joue les modèles de vents en déplaçant une particule au cours du temps dans la scène 3D tout en affichant la topographie de la région du Pays-d’Enhaut
-  fonctionne avec un casque de réalité virtuelle afin d’améliorer la perception des vents

Pour ce projet, nous avons accès à un pilote professionnel qui connait précisément les différentes situations de vents de la région.

Le projet peut être poursuivi en travail de bachelor.



## Tâches
Travaux à réaliser:

-  Elaborer le cahier des charges en étroite collaboration avec les responsables du projet.
-  Choix de la plateforme pour le simulateur et le casque de réalité virtuelle et du (des) logiciel(s) pour le développement
-  Définition du fonctionnement et du graphisme du simulateur en y intégrant le casque de réalité virtuelle.
-  Développer le simulateur
-  Elaborer une documentation technique complète et rédaction d’un rapport décrivant la démarche et les choix opérés.
